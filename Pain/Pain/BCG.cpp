#include "Solver.h"

void Solver::BCG_Diag()
{
    double disc;
    complex<double> alpha, betta, a1, a2, b1;
    complex<double> *rs, *r_rs;

    complex<double> *r_star = new complex<double>[n];
    complex<double> *p_star = new complex<double>[n];
    complex<double> *w_star = new complex<double>[n];
    complex<double> *a_star = new complex<double>[n];

    if (_smooth)
    {
        cout << "BCG_Diag_Smooth..." << endl;
        log.open("BCG_Diag_Smooth.txt");
    }
    else
    {
        cout << "BCG_Diag..." << endl;
        log.open("BCG_Diag.txt");
    }

    time_start = clock();

    multMonV(x, r);

    for (int i = 0; i < n; i++)
    {
        r[i] = b[i] - r[i];
        r_star[i] = r[i];

        p[i] = r[i] / di[i];
        w[i] = p[i];
    }

    multMonV(p, a);

    for (int i = 0; i < n; i++)
    {
        complex<double> tmp;
        tmp = complex<double>(di[i].real(), -1.0 * di[i].imag());

        p_star[i] = r_star[i] / tmp;
        w_star[i] = p_star[i];
    }

    multMTonV(p_star, a_star);

    if (_smooth)
    {
        y = new complex<double>[n];
        rs = new complex<double>[n];
        r_rs = new complex<double>[n];

        for (int i = 0; i < n; i++)
        {
            rs[i] = r[i];
            y[i] = x[i];
        }
    }

    r0 = sqrt(scalarReal(r, r));

    if (r0 < 1e-30)
    {
        log << "x0 is solution" << endl;
        if (_debug) cout << "x0 is solution" << endl;
        return;
    }

    for (int iter = 1; iter <= maxIter; iter++)
    {
        a1 = scalarReal(w_star, r);
        a2 = scalarReal(p_star, a);

        if (fabs(real(a1)) < 1e-30 && fabs(imag(a1)) < 1e-30)
        {
            cout << "alpha = 0, breaking" << endl;
            log << "alpha = 0, breaking" << endl;
            break;
        }

        if (fabs(real(a2)) < 1e-30 && fabs(imag(a2)) < 1e-30)
        {
            cout << "alpha: division by 0, breaking" << endl;
            log << "alpha: division by 0, breaking" << endl;
            break;
        }

        alpha = a1 / a2;

        for (int i = 0; i < n; i++)
        {
            x[i] += alpha * p[i];
            r[i] -= alpha * a[i];
            r_star[i] -= alpha * a_star[i];
        }        

        if (_smooth)
        {
            double nu;

            for (int i = 0; i < n; i++)
            {
                r_rs[i] = r[i] - rs[i];
            }

            nu = -1.0 * scalarReal(rs, r_rs) / scalarReal(r_rs, r_rs);

            if (nu < 0.0) nu = 0.0;
            if (nu > 1.0) nu = 1.0;

            for (int i = 0; i < n; i++)
            {
                y[i] = (1.0 - nu) * y[i] + nu * x[i];
                rs[i] = (1.0 - nu) * rs[i] + nu * r[i];
            }

            disc = sqrt(scalarReal(rs, rs)) / r0;
        }
        else
        {
            disc = sqrt(scalarReal(r, r)) / r0;
        }

        log << iter << "\t" << scientific << disc << endl;
        if (_debug) cout << iter << "\t" << scientific << disc << endl;

        if (disc < eps)
        {
            break;
        }

        for (int i = 0; i < n; i++)
        {
            w[i] = r[i] / di[i];

            complex<double> tmp;
            tmp = complex<double>(di[i].real(), -1.0 * di[i].imag());

            w_star[i] = r_star[i] / tmp;
        }

        b1 = scalarReal(w_star, r);

        betta = b1 / a1;

        for (int i = 0; i < n; i++)
        {
            p[i] = w[i] + betta * p[i];
            p_star[i] = w_star[i] + betta * p_star[i];
        }

        multMonV(p, a);
        multMTonV(p_star, a_star);
    }

    time_stop = clock();
    time = ((double)time_stop - time_start) / ((double)CLOCKS_PER_SEC);
    cout << "Solution time:\t" << time << endl;
    log << "Solution time:\t" << time << endl;
    log.close();

    delete[] r_star;
    delete[] p_star;
    delete[] w_star;
    delete[] a_star;

    if (_smooth)
    {
        delete[] rs;
        delete[] r_rs;
    }
}

void Solver::BCG_SSOR()
{
    double disc;
    complex<double> alpha, betta, a1, a2, b1;
    complex<double> *rs, *r_rs;

    complex<double> *r_star = new complex<double>[n];
    complex<double> *p_star = new complex<double>[n];
    complex<double> *w_star = new complex<double>[n];
    complex<double> *a_star = new complex<double>[n];

    if (_smooth)
    {
        cout << "BCG_SSOR_Smooth..." << endl;
        log.open("BCG_SSOR_Smooth.txt");
    }
    else
    {
        cout << "BCG_SSOR..." << endl;
        log.open("BCG_SSOR.txt");
    }

    time_start = clock();

    multMonV(x, r);

    for (int i = 0; i < n; i++)
    {
        r[i] = b[i] - r[i];
        r_star[i] = r[i];
    }

    solveSSOR(r, p);

    for (int i = 0; i < n; i++)
    {
        w[i] = p[i];
    }

    multMonV(p, a);

    solveSSOR_star(r_star, p_star);

    for (int i = 0; i < n; i++)
    {
        w_star[i] = p_star[i];
    }

    multMTonV(p_star, a_star);

    if (_smooth)
    {
        y = new complex<double>[n];
        rs = new complex<double>[n];
        r_rs = new complex<double>[n];

        for (int i = 0; i < n; i++)
        {
            rs[i] = r[i];
            y[i] = x[i];
        }
    }

    r0 = sqrt(scalarReal(r, r));

    if (r0 < 1e-30)
    {
        log << "x0 is solution" << endl;
        if (_debug) cout << "x0 is solution" << endl;
        return;
    }

    for (int iter = 1; iter <= maxIter; iter++)
    {
        a1 = scalarReal(w_star, r);
        a2 = scalarReal(p_star, a);

        if (fabs(real(a1)) < 1e-30 && fabs(imag(a1)) < 1e-30)
        {
            cout << "alpha = 0, breaking" << endl;
            log << "alpha = 0, breaking" << endl;
            break;
        }

        if (fabs(real(a2)) < 1e-30 && fabs(imag(a2)) < 1e-30)
        {
            cout << "alpha: division by 0, breaking" << endl;
            log << "alpha: division by 0, breaking" << endl;
            break;
        }

        alpha = a1 / a2;

        for (int i = 0; i < n; i++)
        {
            x[i] += alpha * p[i];
            r[i] -= alpha * a[i];
            r_star[i] -= alpha * a_star[i];
        }        

        if (_smooth)
        {
            double nu;

            for (int i = 0; i < n; i++)
            {
                r_rs[i] = r[i] - rs[i];
            }

            nu = -1.0 * scalarReal(rs, r_rs) / scalarReal(r_rs, r_rs);

            if (nu < 0.0) nu = 0.0;
            if (nu > 1.0) nu = 1.0;

            for (int i = 0; i < n; i++)
            {
                y[i] = (1.0 - nu) * y[i] + nu * x[i];
                rs[i] = (1.0 - nu) * rs[i] + nu * r[i];
            }

            disc = sqrt(scalarReal(rs, rs)) / r0;
        }
        else
        {
            disc = sqrt(scalarReal(r, r)) / r0;
        }

        log << iter << "\t" << scientific << disc << endl;
        if (_debug) cout << iter << "\t" << scientific << disc << endl;

        if (disc < eps)
        {
            break;
        }

        solveSSOR(r, w);
        solveSSOR_star(r_star, w_star);

        b1 = scalarReal(w_star, r);

        betta = b1 / a1;

        for (int i = 0; i < n; i++)
        {
            p[i] = w[i] + betta * p[i];
            p_star[i] = w_star[i] + betta * p_star[i];
        }

        multMonV(p, a);
        multMTonV(p_star, a_star);
    }

    time_stop = clock();
    time = ((double)time_stop - time_start) / ((double)CLOCKS_PER_SEC);
    cout << "Solution time:\t" << time << endl;
    log << "Solution time:\t" << time << endl;
    log.close();

    delete[] r_star;
    delete[] p_star;
    delete[] w_star;
    delete[] a_star;

    if (_smooth)
    {
        delete[] rs;
        delete[] r_rs;
    }
}

void Solver::BCG_LDLT()
{
    double disc;
    complex<double> alpha, betta, a1, a2, b1;
    complex<double> *rs, *r_rs;

    complex<double> *r_star = new complex<double>[n];
    complex<double> *p_star = new complex<double>[n];
    complex<double> *w_star = new complex<double>[n];
    complex<double> *a_star = new complex<double>[n];

    if (_smooth)
    {
        cout << "BCG_LDLT_Smooth..." << endl;
        log.open("BCG_LDLT_Smooth.txt");
    }
    else
    {
        cout << "BCG_LDLT..." << endl;
        log.open("BCG_LDLT.txt");
    }

    time_start = clock();

    multMonV(x, r);
    makeLDLT();
    makeLDLT_star();

    for (int i = 0; i < n; i++)
    {
        r[i] = b[i] - r[i];
        r_star[i] = r[i];
    }

    solveLDLT(r, p);

    for (int i = 0; i < n; i++)
    {
        w[i] = p[i];
    }

    multMonV(p, a);

    solveLDLT_star(r_star, p_star);

    for (int i = 0; i < n; i++)
    {
        w_star[i] = p_star[i];
    }

    multMTonV(p_star, a_star);

    if (_smooth)
    {
        y = new complex<double>[n];
        rs = new complex<double>[n];
        r_rs = new complex<double>[n];

        for (int i = 0; i < n; i++)
        {
            rs[i] = r[i];
            y[i] = x[i];
        }
    }

    r0 = sqrt(scalarReal(r, r));

    if (r0 < 1e-30)
    {
        log << "x0 is solution" << endl;
        if (_debug) cout << "x0 is solution" << endl;
        return;
    }

    for (int iter = 1; iter <= maxIter; iter++)
    {
        a1 = scalarReal(w_star, r);
        a2 = scalarReal(p_star, a);

        if (fabs(real(a1)) < 1e-30 && fabs(imag(a1)) < 1e-30)
        {
            cout << "alpha = 0, breaking" << endl;
            log << "alpha = 0, breaking" << endl;
            break;
        }

        if (fabs(real(a2)) < 1e-30 && fabs(imag(a2)) < 1e-30)
        {
            cout << "alpha: division by 0, breaking" << endl;
            log << "alpha: division by 0, breaking" << endl;
            break;
        }

        alpha = a1 / a2;

        for (int i = 0; i < n; i++)
        {
            x[i] += alpha * p[i];
            r[i] -= alpha * a[i];
            r_star[i] -= alpha * a_star[i];
        }        

        if (_smooth)
        {
            double nu;

            for (int i = 0; i < n; i++)
            {
                r_rs[i] = r[i] - rs[i];
            }

            nu = -1.0 * scalarReal(rs, r_rs) / scalarReal(r_rs, r_rs);

            if (nu < 0.0) nu = 0.0;
            if (nu > 1.0) nu = 1.0;

            for (int i = 0; i < n; i++)
            {
                y[i] = (1.0 - nu) * y[i] + nu * x[i];
                rs[i] = (1.0 - nu) * rs[i] + nu * r[i];
            }

            disc = sqrt(scalarReal(rs, rs)) / r0;
        }
        else
        {
            disc = sqrt(scalarReal(r, r)) / r0;
        }

        log << iter << "\t" << scientific << disc << endl;
        if (_debug) cout << iter << "\t" << scientific << disc << endl;

        if (disc < eps)
        {
            break;
        }

        solveLDLT(r, w);
        solveLDLT_star(r_star, w_star);

        b1 = scalarReal(w_star, r);

        betta = b1 / a1;

        for (int i = 0; i < n; i++)
        {
            p[i] = w[i] + betta * p[i];
            p_star[i] = w_star[i] + betta * p_star[i];
        }

        multMonV(p, a);
        multMTonV(p_star, a_star);
    }

    time_stop = clock();
    time = ((double)time_stop - time_start) / ((double)CLOCKS_PER_SEC);
    cout << "Solution time:\t" << time << endl;
    log << "Solution time:\t" << time << endl;
    log.close();

    delete[] r_star;
    delete[] p_star;
    delete[] w_star;
    delete[] a_star;

    if (_smooth)
    {
        delete[] rs;
        delete[] r_rs;
    }
}