#include "Solver.h"

void Solver::LOS_Diag()
{
    double disc;
    complex<double> alpha, betta, a1, a2, b1;
    complex<double> *rs, *r_rs;

    if (_smooth)
    {
        cout << "LOS_Diag_Smooth..." << endl;
        log.open("LOS_Diag_Smooth.txt");
    }
    else
    {
        cout << "LOS_Diag..." << endl;
        log.open("LOS_Diag.txt");
    }

    time_start = clock();

    multMonV(x, r);

    for (int i = 0; i < n; i++)
    {
        r[i] = b[i] - r[i];
        p[i] = r[i] / di[i];
        s[i] = p[i];
    }

    multMonV(p, a);

    for (int i = 0; i < n; i++)
    {
        z[i] = a[i];
        w[i] = z[i] / di[i];
    }

    if (_smooth)
    {
        y = new complex<double>[n];
        rs = new complex<double>[n];
        r_rs = new complex<double>[n];

        for (int i = 0; i < n; i++)
        {
            rs[i] = r[i];
            y[i] = x[i];
        }
    }

    r0 = sqrt(scalarReal(r, r));

    if (r0 < 1e-30)
    {
        log << "x0 is solution" << endl;
        if (_debug) cout << "x0 is solution" << endl;
        return;
    }

    for (int iter = 1; iter <= maxIter; iter++)
    {
        a1 = scalarReal(w, r);
        a2 = scalarReal(w, z);

        if (fabs(real(a1)) < 1e-30 && fabs(imag(a1)) < 1e-30)
        {
            cout << "alpha = 0, breaking" << endl;
            log << "alpha = 0, breaking" << endl;
            break;
        }

        if (fabs(real(a2)) < 1e-30 && fabs(imag(a2)) < 1e-30)
        {
            cout << "alpha: division by 0, breaking" << endl;
            log << "alpha: division by 0, breaking" << endl;
            break;
        }

        alpha = a1 / a2;

        for (int i = 0; i < n; i++)
        {
            x[i] += alpha * p[i];
            r[i] -= alpha * z[i];
            s[i] -= alpha * w[i];
        }

        multMonV(s, a);        

        if (_smooth)
        {
            double nu;

            for (int i = 0; i < n; i++)
            {
                r_rs[i] = r[i] - rs[i];
            }

            nu = -1.0 * scalarReal(rs, r_rs) / scalarReal(r_rs, r_rs);

            if (nu < 0.0) nu = 0.0;
            if (nu > 1.0) nu = 1.0;

            for (int i = 0; i < n; i++)
            {
                y[i] = (1.0 - nu) * y[i] + nu * x[i];
                rs[i] = (1.0 - nu) * rs[i] + nu * r[i];
            }

            disc = sqrt(scalarReal(rs, rs)) / r0;
        }
        else
        {
            disc = sqrt(scalarReal(r, r)) / r0;
        }

        log << iter << "\t" << scientific << disc << endl;
        if (_debug) cout << iter << "\t" << scientific << disc << endl;

        if (disc < eps)
        {
            break;
        }

        b1 = scalarReal(w, a);

        betta = -b1 / a2;

        for (int i = 0; i < n; i++)
        {
            p[i] = s[i] + betta * p[i];
            z[i] = a[i] + betta * z[i];
            w[i] = z[i] / di[i];
        }
    }

    time_stop = clock();
    time = ((double)time_stop - time_start) / ((double)CLOCKS_PER_SEC);
    cout << "Solution time:\t" << time << endl;
    log << "Solution time:\t" << time << endl;
    log.close();

    if (_smooth)
    {
        delete[] rs;
        delete[] r_rs;
    }
}

void Solver::LOS_SSOR()
{
    double disc;
    complex<double> alpha, betta, a1, a2, b1;
    complex<double> *rs, *r_rs;

    if (_smooth)
    {
        cout << "LOS_SSOR_Smooth..." << endl;
        log.open("LOS_SSOR_Smooth.txt");
    }
    else
    {
        cout << "LOS_SSOR..." << endl;
        log.open("LOS_SSOR.txt");
    }

    time_start = clock();

    multMonV(x, r);

    for (int i = 0; i < n; i++)
    {
        r[i] = b[i] - r[i];
    }

    solveSSOR(r, p);

    for (int i = 0; i < n; i++)
    {
        s[i] = p[i];
    }

    multMonV(p, a);

    for (int i = 0; i < n; i++)
    {
        z[i] = a[i];
    }

    solveSSOR(z, w);

    if (_smooth)
    {
        y = new complex<double>[n];
        rs = new complex<double>[n];
        r_rs = new complex<double>[n];

        for (int i = 0; i < n; i++)
        {
            rs[i] = r[i];
            y[i] = x[i];
        }
    }

    r0 = sqrt(scalarReal(r, r));

    if (r0 < 1e-30)
    {
        log << "x0 is solution" << endl;
        if (_debug) cout << "x0 is solution" << endl;
        return;
    }

    for (int iter = 1; iter <= maxIter; iter++)
    {
        a1 = scalarReal(w, r);
        a2 = scalarReal(w, z);

        if (fabs(real(a1)) < 1e-30 && fabs(imag(a1)) < 1e-30)
        {
            cout << "alpha = 0, breaking" << endl;
            log << "alpha = 0, breaking" << endl;
            break;
        }

        if (fabs(real(a2)) < 1e-30 && fabs(imag(a2)) < 1e-30)
        {
            cout << "alpha: division by 0, breaking" << endl;
            log << "alpha: division by 0, breaking" << endl;
            break;
        }

        alpha = a1 / a2;

        for (int i = 0; i < n; i++)
        {
            x[i] += alpha * p[i];
            r[i] -= alpha * z[i];
            s[i] -= alpha * w[i];
        }

        multMonV(s, a);        

        if (_smooth)
        {
            double nu;

            for (int i = 0; i < n; i++)
            {
                r_rs[i] = r[i] - rs[i];
            }

            nu = -1.0 * scalarReal(rs, r_rs) / scalarReal(r_rs, r_rs);

            if (nu < 0.0) nu = 0.0;
            if (nu > 1.0) nu = 1.0;

            for (int i = 0; i < n; i++)
            {
                y[i] = (1.0 - nu) * y[i] + nu * x[i];
                rs[i] = (1.0 - nu) * rs[i] + nu * r[i];
            }

            disc = sqrt(scalarReal(rs, rs)) / r0;
        }
        else
        {
            disc = sqrt(scalarReal(r, r)) / r0;
        }

        log << iter << "\t" << scientific << disc << endl;
        if (_debug) cout << iter << "\t" << scientific << disc << endl;

        if (disc < eps)
        {
            break;
        }

        b1 = scalarReal(w, a);

        betta = -b1 / a2;

        for (int i = 0; i < n; i++)
        {
            p[i] = s[i] + betta * p[i];
            z[i] = a[i] + betta * z[i];
        }

        solveSSOR(z, w);
    }

    time_stop = clock();
    time = ((double)time_stop - time_start) / ((double)CLOCKS_PER_SEC);
    cout << "Solution time:\t" << time << endl;
    log << "Solution time:\t" << time << endl;
    log.close();

    if (_smooth)
    {
        delete[] rs;
        delete[] r_rs;
    }
}

void Solver::LOS_LLT()
{
    double disc;
    complex<double> alpha, betta, a1, a2, b1;
    complex<double> *rs, *r_rs;

    if (_smooth)
    {
        cout << "LOS_LLT_Smooth..." << endl;
        log.open("LOS_LLT_Smooth.txt");
    }
    else
    {
        cout << "LOS_LLT..." << endl;
        log.open("LOS_LLT.txt");
    }

    time_start = clock();

    multMonV(x, r);

    makeLLT();

    for (int i = 0; i < n; i++)
    {
        r[i] = b[i] - r[i];
    }

    solveLLT(r, p);

    for (int i = 0; i < n; i++)
    {
        s[i] = p[i];
    }

    multMonV(p, a);

    for (int i = 0; i < n; i++)
    {
        z[i] = a[i];
    }

    solveLLT(z, w);

    if (_smooth)
    {
        y = new complex<double>[n];
        rs = new complex<double>[n];
        r_rs = new complex<double>[n];

        for (int i = 0; i < n; i++)
        {
            rs[i] = r[i];
            y[i] = x[i];
        }
    }

    r0 = sqrt(scalarReal(r, r));

    if (r0 < 1e-30)
    {
        log << "x0 is solution" << endl;
        if (_debug) cout << "x0 is solution" << endl;
        return;
    }

    for (int iter = 1; iter <= maxIter; iter++)
    {
        a1 = scalarReal(w, r);
        a2 = scalarReal(w, z);

        if (fabs(real(a1)) < 1e-30 && fabs(imag(a1)) < 1e-30)
        {
            cout << "alpha = 0, breaking" << endl;
            log << "alpha = 0, breaking" << endl;
            break;
        }

        if (fabs(real(a2)) < 1e-30 && fabs(imag(a2)) < 1e-30)
        {
            cout << "alpha: division by 0, breaking" << endl;
            log << "alpha: division by 0, breaking" << endl;
            break;
        }

        alpha = a1 / a2;

        for (int i = 0; i < n; i++)
        {
            x[i] += alpha * p[i];
            r[i] -= alpha * z[i];
            s[i] -= alpha * w[i];
        }

        multMonV(s, a);        

        if (_smooth)
        {
            double nu;

            for (int i = 0; i < n; i++)
            {
                r_rs[i] = r[i] - rs[i];
            }

            nu = -1.0 * scalarReal(rs, r_rs) / scalarReal(r_rs, r_rs);

            if (nu < 0.0) nu = 0.0;
            if (nu > 1.0) nu = 1.0;

            for (int i = 0; i < n; i++)
            {
                y[i] = (1.0 - nu) * y[i] + nu * x[i];
                rs[i] = (1.0 - nu) * rs[i] + nu * r[i];
            }

            disc = sqrt(scalarReal(rs, rs)) / r0;
        }
        else
        {
            disc = sqrt(scalarReal(r, r)) / r0;
        }

        log << iter << "\t" << scientific << disc << endl;
        if (_debug) cout << iter << "\t" << scientific << disc << endl;

        if (disc < eps)
        {
            break;
        }

        b1 = scalarReal(w, a);

        betta = -b1 / a2;

        for (int i = 0; i < n; i++)
        {
            p[i] = s[i] + betta * p[i];
            z[i] = a[i] + betta * z[i];
        }

        solveLLT(z, w);
    }

    time_stop = clock();
    time = ((double)time_stop - time_start) / ((double)CLOCKS_PER_SEC);
    cout << "Solution time:\t" << time << endl;
    log << "Solution time:\t" << time << endl;
    log.close();

    if (_smooth)
    {
        delete[] rs;
        delete[] r_rs;
    }
}