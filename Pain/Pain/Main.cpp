#include <cstdio>
#include <string>
#include <complex>

#include "Solver.h"

using namespace std;

void main()
{
    bool smooth = false;
    bool txt = false;
    bool debug = true;
    int comm;

	Solver *solver = new Solver("matrices/1", smooth, txt, debug);	
	cout << "Reading complete" << endl;

    cout << "1. LOSCx_DIAG.\t2. LOSCx_SSOR.\t3. LOSCx_LLT.\t4. LOSCx_LDLT" << endl;
    cout << "5. LOS_DIAG.\t6. LOS_SSOR.\t7. LOS_LLT." << endl;
    cout << "8. BCG_DIAG.\t9. BCG_SSOR.\t10. BCG_LDLT." << endl;
	cin >> comm;

    switch (comm)
    {
    case 1:
    {        
        solver->LOSCx_Diag();        
        break;
    }
    case 2:
    {
        solver->LOSCx_SSOR();
        break;
    }
    case 3:
    {
        solver->LOSCx_LLT();
        break;
    }
    case 4:
    {
        solver->LOSCx_LDLT();
        break;
    }
    case 5:
    {        
        solver->LOS_Diag();
        break;
    }
    case 6:
    {
        solver->LOS_SSOR();
        break;
    }
    case 7:
    {
        solver->LOS_LLT();
        break;
    }
    case 8:
    {        
        solver->BCG_Diag();
        break;
    }
    case 9:
    {
        solver->BCG_SSOR();
        break;
    }
    case 10:
    {
        solver->BCG_LDLT();
        break;
    }
    default:
        break;
    }

    cout << solver->getDiscrepancy() << endl;

	system("pause");
	delete solver;
}