#include "Solver.h"

bool Solver::readBin(string path)
{
	FILE *fin;
    int val;
    double re, im;

	fin = fopen(string(path + "/kuslau").c_str(), "r");

    if (fin == NULL)
    {
        cout << "Reading error: kuslau" << endl;
        return false;
    }

	fscanf(fin, "%d%lf%d", &n, &eps, &maxIter);
	fclose(fin);

	n /= 2;
		
	// idi
	idi = new int[n + 1];

	fin = fopen((path + "/idi").c_str(), "rb");

    if (fin == NULL)
    {
        cout << "Reading error: idi" << endl;
        return false;
    }

	for (int i = 0; i < n + 1; i++)
	{
		fread(&val, sizeof(val), 1, fin);
		idi[i] = val - 1;
	}
	fclose(fin);

	// di
	di = new complex<double>[n];	

	fin = fopen((path + "/di").c_str(), "rb");

    if (fin == NULL)
    {
        cout << "Reading error: di" << endl;
        return false;
    }

	for (int i = 0, j = 0; i < idi[n]; i++, j++)
	{
		fread(&re, sizeof(re), 1, fin);

		if (idi[j + 1] - idi[j] == 2)
		{
			fread(&im, sizeof(im), 1, fin);
			di[j].imag(im);
			i++;
		}

		di[j].real(re);
	}
	fclose(fin);

	// ig
	ig = new int[n + 1];

	fin = fopen((path + "/ig").c_str(), "rb");

    if (fin == NULL)
    {
        cout << "Reading error: ig" << endl;
        return false;
    }

	for (int i = 0; i < n + 1; i++)
	{
		fread(&val, sizeof(val), 1, fin);
		ig[i] = val - 1;
	}
	fclose(fin);

	// jg
	jg = new int[ig[n]];

	fin = fopen((path + "/jg").c_str(), "rb");

    if (fin == NULL)
    {
        cout << "Reading error: jg" << endl;
        return false;
    }

	for (int i = 0; i < ig[n]; i++)
	{
		fread(&val, sizeof(val), 1, fin);
		jg[i] = val - 1;
	}
	fclose(fin);

	// ijg
	ijg = new int[ig[n] + 1];

	fin = fopen((path + "/ijg").c_str(), "rb");

    if (fin == NULL)
    {
        cout << "Reading error: ijg" << endl;
        return false;
    }

	for (int i = 0; i < ig[n] + 1; i++)
	{
		fread(&val, sizeof(val), 1, fin);
		ijg[i] = val - 1;
	}
	fclose(fin);

	//gg
	gg = new complex<double>[ig[n]];

	fin = fopen((path + "/gg").c_str(), "rb");

    if (fin == NULL)
    {
        cout << "Reading error: gg" << endl;
        return false;
    }

	for (int i = 0, j = 0; i < ijg[ig[n]]; i++, j++)
	{
		fread(&re, sizeof(re), 1, fin);

		if (ijg[j + 1] - ijg[j] == 2)
		{
			fread(&im, sizeof(im), 1, fin);
			gg[j].imag(im);
			i++;
		}
		gg[j].real(re);
	}
	fclose(fin);


	b = new complex<double>[n];

	fin = fopen((path + "/pr").c_str(), "rb");

    if (fin == NULL)
    {
        cout << "Reading error: pr" << endl;
        return false;
    }

	for (int i = 0; i < n; i++)
	{
		fread(&re, sizeof(re), 1, fin);
		b[i].real(re);

		fread(&im, sizeof(im), 1, fin);
		b[i].imag(im);
	}
	fclose(fin);

	return true;
}

bool Solver::readTxt(string path)
{
	FILE *fin;
    int val;
    double re, im;

	fin = fopen(string(path + "/kuslau.txt").c_str(), "r");

    if (fin == NULL)
    {
        cout << "Reading error: kuslau" << endl;
        return false;
    }

	fscanf(fin, "%d%lf%d", &n, &eps, &maxIter);
	fclose(fin);

	n /= 2;

	// idi
	idi = new int[n + 1];

	fin = fopen((path + "/idi.txt").c_str(), "r");

    if (fin == NULL)
    {
        cout << "Reading error: idi" << endl;
        return false;
    }

	for (int i = 0; i < n + 1; i++)
	{		
        fscanf(fin, "%d", &val);
		idi[i] = val - 1;
	}
	fclose(fin);

	// di
	di = new complex<double>[n];

	fin = fopen((path + "/di.txt").c_str(), "r");

    if (fin == NULL)
    {
        cout << "Reading error: di" << endl;
        return false;
    }

	for (int i = 0, j = 0; i < idi[n]; i++, j++)
	{
        fscanf(fin, "%lf", &re);

		if (idi[j + 1] - idi[j] == 2)
		{
            fscanf(fin, "%lf", &im);
			di[j].imag(im);
			i++;
		}
		di[j].real(re);
	}
	fclose(fin);

	// ig
	ig = new int[n + 1];

	fin = fopen((path + "/ig.txt").c_str(), "r");

    if (fin == NULL)
    {
        cout << "Reading error: ig" << endl;
        return false;
    }

	for (int i = 0; i < n + 1; i++)
	{
        fscanf(fin, "%d", &val);
		ig[i] = val - 1;
	}
	fclose(fin);

	// jg
	jg = new int[ig[n]];

	fin = fopen((path + "/jg.txt").c_str(), "r");

    if (fin == NULL)
    {
        cout << "Reading error: jg" << endl;
        return false;
    }

	for (int i = 0; i < ig[n]; i++)
	{
        fscanf(fin, "%d", &val);
		jg[i] = val - 1;
	}
	fclose(fin);

	// ijg
	ijg = new int[ig[n] + 1];

	fin = fopen((path + "/ijg.txt").c_str(), "r");

    if (fin == NULL)
    {
        cout << "Reading error: ijg" << endl;
        return false;
    }

	for (int i = 0; i < ig[n] + 1; i++)
	{
        fscanf(fin, "%d", &val);
		ijg[i] = val - 1;
	}
	fclose(fin);

	//gg
	gg = new complex<double>[ig[n]];

	fin = fopen((path + "/gg.txt").c_str(), "r");

    if (fin == NULL)
    {
        cout << "Reading error: gg" << endl;
        return false;
    }

	for (int i = 0, j = 0; i < ijg[ig[n]]; i++, j++)
	{
        fscanf(fin, "%lf", &re);

		if (ijg[j + 1] - ijg[j] == 2)
		{
            fscanf(fin, "%lf", &im);
			gg[j].imag(im);
			i++;
		}
		gg[j].real(re);
	}
	fclose(fin);

	b = new complex<double>[n];

	fin = fopen((path + "/pr.txt").c_str(), "r");

    if (fin == NULL)
    {
        cout << "Reading error: pr" << endl;
        return false;
    }

	for (int i = 0; i < n; i++)
	{
		fscanf(fin, "%lf", &re);
		b[i].real(re);

		fscanf(fin, "%lf", &im);
		b[i].imag(im);
	}
	fclose(fin);

	return true;
}

Solver::Solver(string path, bool smooth, bool text, bool debug)
{
    _smooth = smooth;
    _debug = debug;

	if (!text) 
	{
        _input = readBin(path);
	}
	else
	{
        _input = readTxt(path);
	}

	if (_input)
	{
		this->x = new complex<double>[n];

		for (int i = 0; i < n; i++)
		{
			x[i] = complex<double>(0.0, 0.0);
		}

		r = new complex<double>[n];
		p = new complex<double>[n];
		s = new complex<double>[n];
		z = new complex<double>[n];
        a = new complex<double>[n];
        w = new complex<double>[n];

		d = new complex<double>[n];
		l = new complex<double>[ig[n]];

        d_star = new complex<double>[n];
        l_star = new complex<double>[ig[n]];

		for (int i = 0; i < n; i++)
		{
			d[i] = di[i];
            d_star[i] = complex<double>(di[i].real(), -1.0 * di[i].imag());
		}

        for (int i = 0; i < ig[n]; i++)
        {
            l[i] = gg[i];
            l_star[i] = complex<double>(gg[i].real(), -1.0 * gg[i].imag());
        }
	}
	else
	{
		cout << "Reading error" << endl;
	}
}

Solver::~Solver()
{
	if (_input)
	{
		delete[] ig;
		delete[] jg;
		delete[] di;
		delete[] gg;  

        delete[] x;
        delete[] b;
		
		delete[] p;
		delete[] s;
        delete[] z;
        delete[] a;
        delete[] w;		

        delete[] l;
        delete[] d;
        delete[] l_star;
        delete[] d_star;
	}
}

complex<double> Solver::scalarComplex(complex<double>* a, complex<double>* b)
{
	complex<double> result = complex<double>(0.0, 0.0);

	for (int i = 0; i < n; i++)
	{
		result += a[i] * b[i];
	}

	return result;
}

double Solver::scalarReal(complex<double>* a, complex<double>* b)
{
	double res = 0.0;

	for (int i = 0; i < n; i++)
	{
		res += a[i].real() * b[i].real();
		res += a[i].imag() * b[i].imag();
	}

	return res;
}

void Solver::multMonV(complex<double>* x, complex<double>* res)
{	
    complex<double> tmp;

	for (int i = 0; i < n; i++)
	{
		tmp = x[i];
		res[i] = di[i] * tmp;

		for (int k = ig[i], k1 = ig[i + 1]; k < k1; k++)
		{
			int j = jg[k];

			res[i] += gg[k] * x[j];
			res[j] += gg[k] * tmp;
		}
	}	
}

void Solver::multMTonV(complex<double>* x, complex<double>* res)
{
    complex<double> tmp, tmp1;

    for (int i = 0; i < n; i++)
    {
        tmp = x[i];      
        tmp1 = complex<double>(di[i].real(), -1.0 * di[i].imag());
        res[i] = tmp1 * tmp;

        for (int k = ig[i], k1 = ig[i + 1]; k < k1; k++)
        {
            int j = jg[k];

            tmp1 = complex<double>(gg[k].real(), -1.0 * gg[k].imag());
            res[i] += tmp1 * x[j];
            res[j] += tmp1 * tmp;
        }
    }
}

/* SSOR */
void Solver::solveSSOR(const complex<double> *f, complex<double> *x)
{
    complex<double> sum;
	complex<double> *tmp = new complex<double>[n];

    for (int i = 0; i < n; i++)
    {
        tmp[i] = f[i];
    }

    // x = tmp / (L + D)
	for (int k = 1, k1 = 0; k <= n; k++, k1++)
	{
		sum = complex<double>(0.0, 0.0);

        for (int i = ig[k1]; i < ig[k]; i++)
        {
            sum += gg[i] * x[jg[i]];
        }

		x[k1] = (tmp[k1] - sum) / di[k1];
	}

	// tmp = D * x
    for (int i = 0; i < n; i++)
    {
        tmp[i] = x[i] * di[i];
    }

	// x = tmp / (L + D)T
	for (int k = n, k1 = n - 1; k > 0; k--, k1--)
	{
		x[k1] = tmp[k1] / di[k1];

        for (int i = ig[k1]; i < ig[k]; i++)
        {
            tmp[jg[i]] -= gg[i] * x[k1];
        }
	}

	delete[] tmp;
}

/* SSOR_star */
void Solver::solveSSOR_star(const complex<double> *f, complex<double> *x)
{
    complex<double> sum, val;
    complex<double> *tmp = new complex<double>[n];

    for (int i = 0; i < n; i++)
    {
        tmp[i] = f[i];
    }
   
    // x = tmp / (L + D)
    for (int k = 1, k1 = 0; k <= n; k++, k1++)
    {
        sum = complex<double>(0.0, 0.0);

        for (int i = ig[k1]; i < ig[k]; i++)
        {
            sum += gg[i] * x[jg[i]];
        }

        x[k1] = (tmp[k1] - sum) / di[k1];

        //val = complex<double>(di[k1].real(), -1.0 * di[k1].imag());
        //x[k1] = (tmp[k1] - sum) / val;        
    }

    // tmp = DT * x
    for (int i = 0; i < n; i++)
    {
        complex<double> val = complex<double>(di[i].real(), -1.0 * di[i].imag());
        tmp[i] = x[i] * val;
    }

    // x = tmp / (L + D)T
    for (int k = n, k1 = n - 1; k > 0; k--, k1--)
    {
        //val = complex<double>(di[k1].real(), -1.0 * di[k1].imag());
        //x[k1] = tmp[k1] / val;

        x[k1] = tmp[k1] / di[k1];

        for (int i = ig[k1]; i < ig[k]; i++)
        {
            tmp[jg[i]] -= gg[i] * x[k1];
        }
    }

    delete[] tmp;
}

/* LLT */
void Solver::makeLLT()
{
    complex<double> sumD, sumL;

    for (int k = 0; k < n; k++)
    {
        sumD = 0;
        int is = ig[k], ie = ig[k + 1];

        for (int i = is; i < ie; i++)
        {
            sumL = 0;
            int js = ig[jg[i]], je = ig[jg[i] + 1];

            for (int m = is; m < i; m++)
            {
                for (int j = js; j < je; j++)
                {
                    if (jg[m] == jg[j])
                    {
                        sumL += l[m] * l[j];
                        js++;
                    }
                }
            }

            l[i] = (l[i] - sumL) / d[jg[i]];
            sumD += l[i] * l[i];
        }

        d[k] = sqrt(d[k] - sumD);
    }
}

void Solver::LLT_solveL(complex<double>* f, complex<double>* x)
{
    complex<double> sum;

    for (int i = 1, i1 = 0; i <= n; i++, i1++)
    {
        sum = complex<double>(0.0, 0.0);

        for (int j = ig[i1]; j < ig[i]; j++)
        {
            sum += l[j] * x[jg[j]];
        }

        x[i1] = (f[i1] - sum) / d[i1];
    }
}

void Solver::LLT_solveU(complex<double>* f, complex<double>* x)
{
    for (int i = n, i1 = n - 1; i > 0; i--, i1--)
    {
        x[i1] = f[i1] / d[i1];

        for (int j = ig[i1]; j < ig[i]; j++)
        {
            f[jg[j]] -= l[j] * x[i1];
        }
    }
}

void Solver::solveLLT(complex<double>* f, complex<double>* x)
{
    LLT_solveL(f, x);
    LLT_solveU(x, x);
}

/* LDLT */
void Solver::makeLDLT()
{
	complex<double> sumD, sumL;

	for (int k = 0; k < n; k++)
	{
		sumD = 0;
		int is = ig[k], ie = ig[k + 1];

		for (int i = is; i < ie; i++)
		{
			sumL = 0;
			int js = ig[jg[i]], je = ig[jg[i] + 1];

			for (int m = is; m < i; m++)
			{
				for (int j = js; j < je; j++)
				{
					if (jg[m] == jg[j])
					{
						sumL += l[m] * l[j] * d[jg[m]];
						js++;
					}
				}
			}

			l[i] = (l[i] - sumL) / d[jg[i]];
			sumD += l[i] * l[i] * d[jg[i]];
		}

		d[k] = d[k] - sumD;
	}
}

void Solver::LDLT_solveL(complex<double>* f, complex<double>* x)
{
    complex<double> sum;

	for (int i = 1, i1 = 0; i <= n; i++, i1++)
	{
		sum = complex<double>(0.0, 0.0);

        for (int j = ig[i1]; j < ig[i]; j++)
        {
            sum += l[j] * x[jg[j]];
        }

		x[i1] = (f[i1] - sum);
	}
}

void Solver::LDLT_solveLT(complex<double>* f, complex<double>* x)
{
	for (int i = n, i1 = n - 1; i > 0; i--, i1--)
	{
		x[i1] = f[i1];

        for (int j = ig[i1]; j < ig[i]; j++)
        {
            f[jg[j]] -= l[j] * x[i1];
        }
	}
}

void Solver::solveLDLT(complex<double>* f, complex<double>* x)
{
	LDLT_solveL(f, x);

	for (int i = 0; i < n; i++)
	{
		x[i] /= d[i];
	}

	LDLT_solveLT(x, x);
}

/* LDLT_star */
void Solver::makeLDLT_star()
{
    complex<double> sumD, sumL;

    for (int k = 0; k < n; k++)
    {
        sumD = 0;
        int is = ig[k], ie = ig[k + 1];

        for (int i = is; i < ie; i++)
        {
            sumL = 0;
            int js = ig[jg[i]], je = ig[jg[i] + 1];

            for (int m = is; m < i; m++)
            {
                for (int j = js; j < je; j++)
                {
                    if (jg[m] == jg[j])
                    {
                        sumL += l_star[m] * l_star[j] * d_star[jg[m]];
                        js++;
                    }
                }
            }

            l_star[i] = (l_star[i] - sumL) / d_star[jg[i]];
            sumD += l_star[i] * l_star[i] * d_star[jg[i]];
        }

        d_star[k] = d_star[k] - sumD;
    }
}

void Solver::LDLT_solveL_star(complex<double>* f, complex<double>* x)
{
    complex<double> sum;

    for (int i = 1, i1 = 0; i <= n; i++, i1++)
    {
        sum = complex<double>(0.0, 0.0);

        for (int j = ig[i1]; j < ig[i]; j++)
        {
            sum += l_star[j] * x[jg[j]];
        }

        x[i1] = (f[i1] - sum);
    }
}

void Solver::LDLT_solveLT_star(complex<double>* f, complex<double>* x)
{
    for (int i = n, i1 = n - 1; i > 0; i--, i1--)
    {
        x[i1] = f[i1];
        
        for (int j = ig[i1]; j < ig[i]; j++)
        {
            f[jg[j]] -= l_star[j] * x[i1];
        }
    }
}

void Solver::solveLDLT_star(complex<double>* f, complex<double>* x)
{
    LDLT_solveL_star(f, x);

    for (int i = 0; i < n; i++)
    {
        x[i] /= d_star[i];
    }

    LDLT_solveLT_star(x, x);
}

double Solver::getDiscrepancy()
{
    double disc;
    complex<double> *r = new complex<double>[n];

    if (_smooth)
    {
        multMonV(y, r);
    }
    else
    {
        multMonV(x, r);
    }

    for (int i = 0; i < n; i++)
    {
        r[i] = b[i] - r[i];
    }

    disc = sqrt(scalarReal(r, r)) / r0;

    return disc;
}