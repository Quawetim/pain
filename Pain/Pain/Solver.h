#include <complex>
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

class Solver
{
private:
    bool _input, _debug, _smooth;
    ofstream log;

	double eps;
	int maxIter;

    clock_t time_start, time_stop;
    double time;

	complex<double> *di, *gg, *b, *r, *x0, *p, *s, *a, *w, *z, *x, *y;
	complex<double> *l, *d, *l_star, *d_star;
	int *idi, *ig, *jg, *ijg, n;	
    double r0;

    bool readBin(string path);
    bool readTxt(string path);

	complex<double> scalarComplex(complex<double> *a, complex<double> *b);
    double scalarReal(complex<double> *a, complex<double> *b);

	void multMonV(complex<double>* x, complex<double>* res);
    void multMTonV(complex<double>* x, complex<double>* res);		
		
	// SSOR
	void solveSSOR(const complex<double> *f, complex<double> *x);

    // SSOR_star
    void solveSSOR_star(const complex<double> *f, complex<double> *x);

    // LLT
    void makeLLT();
    void LLT_solveL(complex<double> *f, complex<double> *x);
    void LLT_solveU(complex<double> *f, complex<double> *x);
    void solveLLT(complex<double> *f, complex<double> *x);

	// LDLT
	void makeLDLT();
	void LDLT_solveL(complex<double> *f, complex<double> *x);
	void LDLT_solveLT(complex<double> *f, complex<double> *x);
	void solveLDLT(complex<double> *f, complex<double> *x);

    // LDLT_star
    void makeLDLT_star();
    void LDLT_solveL_star(complex<double> *f, complex<double> *x);
    void LDLT_solveLT_star(complex<double> *f, complex<double> *x);
    void solveLDLT_star(complex<double> *f, complex<double> *x);

public:

    Solver(string path, bool smooth, bool text, bool debug = false);
	~Solver();

    double getDiscrepancy();

    /* LOSCx */
	void LOSCx_Diag();
	void LOSCx_SSOR();	
	void LOSCx_LLT();
	void LOSCx_LDLT();

    /* LOS */
    void LOS_Diag();
    void LOS_SSOR();
    void LOS_LLT();

    /* BCG */
    void BCG_Diag();
    void BCG_SSOR();
    void BCG_LDLT();
};

